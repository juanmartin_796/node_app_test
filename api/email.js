import * as nodemailer from 'nodemailer';

import * as dotenv from 'dotenv'
import send from '../service/service_email.js';
dotenv.config()

export default async (req, res) => {
    if (req.method === "POST") {
        const required = ["email", "fullname", "message"]
        const {email, fullname, message} = req.body
        const missingKeys = required.filter(k => !Object.keys(req.body).includes(k))
        console.log("Pinchi keys: ", missingKeys)
        if (missingKeys.length > 0){
            return res.status(400).json({success: false, error: `Missing keys: ${missingKeys.toString()}`})
        }
        console.log("Entro en el POST");
        const body = `
        <h3> New contact company from website form </h3>
        <p><b>Full Name:</b> ${fullname}</p>
        <p><b>Email:</b> ${email}<p/>
        <p><b>Message:</b> <q>${message}</q></p>
        `
        const sentRes = await send(email, process.env.FROM_EMAIL, "New contact from website 😃", body)
        console.log("Pinchi response", sentRes)
        res.status(200).json({ success: true, data: sentRes });
    } else if (req.method === "GET") {
        res.status(200).json({ sucess: true });
    } else if (req.method === "OPTIONS") {
        res.status(200).json({ sucess: true });
    } else {
        res.status(404).json({ status: "Error route note found" });
    }
}   