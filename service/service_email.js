import * as nodemailer from 'nodemailer';

import * as dotenv from 'dotenv'
dotenv.config()

const send = async (to, from, subject, html) => {
  let transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com", 
    port: 587,
    secure: false, // true for 465, false for other ports
    secureConnection: false,
    tls: {
      ciphers:'SSLv3'
    },
    auth: {
      user: process.env.FROM_EMAIL,
      pass: process.env.PASSWORD,
    },
  });

  let info = await transporter.sendMail({
    from, 
    to, 
    subject, 
    // text: "Hello world?", // plain text body
    html, // html body
  });

  return {
    accepted: info.accepted,
    rejected: info.rejected
  }

};

export default send
